package com.uriolus.grid.presentation.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.uriolus.grid.R
import com.uriolus.infrastructure.domain.Picture


class PictureAdapter : RecyclerView.Adapter<PictureAdapter.PictureViewHolder>() {

    private val pictures = mutableListOf<Picture>()

    fun setPictures(pictures:List<Picture>){
        this.pictures.clear()
        this.pictures.addAll(pictures)
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        return PictureViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.grid_item_layout, parent, false))
    }

    override fun getItemCount(): Int =
        pictures.size

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        holder.bind(pictures[position])
    }

    class PictureViewHolder(
        private val view: View
    ) :
        RecyclerView.ViewHolder(
            view
        ) {
        private var pictureImageView: ImageView = view.findViewById(R.id.item_picture)

        fun bind(picture: Picture) {
            Glide.with(view).load(picture.path).into(pictureImageView)
        }
    }
}
