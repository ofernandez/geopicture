package com.uriolus.grid.presentation.di

import com.uriolus.infrastructure.di.coreComponent
import org.kodein.di.Kodein

internal val featureComponent = Kodein {
    extend(coreComponent)
    import(featureModule)
}
