package com.uriolus.grid.presentation.di

import com.uriolus.grid.presentation.presenter.GridPresenter
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider


val featureModule = Kodein.Module(name = "GRID_MODULE") {
    bind<GridPresenter>() with provider { GridPresenter(pictureRepository = instance()) }
}
