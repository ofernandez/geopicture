package com.uriolus.grid.presentation.presenter

import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.PictureRepository
import com.uriolus.infrastructure.domain.exceptions.error.DomainError
import com.uriolus.infrastructure.threading.LifecyclePresenter
import com.uriolus.infrastructure.threading.ViewPresenter
import kotlinx.coroutines.launch


class GridPresenter(
    val pictureRepository: PictureRepository
) : LifecyclePresenter<GridPresenter.View>() {

    override fun initPresenter() {
        view?.let {
            if (it.checkPermissionsGranted()) {
                getPictures()
            } else {
                view?.askForPermissions()
            }
        }
    }

    fun permissionsHaveBeenGranted() {
        getPictures()
    }

    fun permissionsHaveNotBeenGranted() {
        view?.showNoContent()
    }

    private fun getPictures() {
       scope.launch {
            pictureRepository.getAllPictures()
                .fold(
                    ifLeft = { view?.renderError(it) },
                    ifRight = {
                        println("PICTURES ok: ${it.size}")
                        view?.renderPictures(it)
                    }
                )
        }
    }

    fun onClose() {
        clear()
    }

    interface View : ViewPresenter {
        fun renderPictures(pictures: List<Picture>)
        fun renderError(error: DomainError)
        fun checkPermissionsGranted(): Boolean
        fun askForPermissions()
        fun showNoContent()
    }
}
