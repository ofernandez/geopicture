package com.uriolus.grid.presentation.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.uriolus.grid.R
import com.uriolus.grid.presentation.di.featureComponent
import com.uriolus.grid.presentation.presenter.GridPresenter
import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.exceptions.error.DomainError
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class GridActivity : AppCompatActivity(), GridPresenter.View {

    private val presenter: GridPresenter by featureComponent.instance()
    private var adapter = PictureAdapter()

    companion object {
        private const val REQUEST_READ_EXTERNAL_STORAGE = 432
        const val NO_SEPARATOR_ORIENTATION = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onViewReady(this)
        setupUI()
    }

    private fun setupUI() {
        val layoutManager = GridLayoutManager(grid_layout.context, 2)
        val dividerItemDecoration = DividerItemDecoration(grid_layout.context, NO_SEPARATOR_ORIENTATION)
        grid_layout.layoutManager = layoutManager
        grid_layout.adapter = adapter
        grid_layout.addItemDecoration(dividerItemDecoration)
    }

    override fun checkPermissionsGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun askForPermissions() {
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                REQUEST_READ_EXTERNAL_STORAGE
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onClose()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    presenter.permissionsHaveBeenGranted()
                } else {
                    presenter.permissionsHaveNotBeenGranted()
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun renderPictures(pictures: List<Picture>) {
        adapter.setPictures(pictures)
    }

    override fun renderError(error: DomainError) {
        println(error)
    }

    override fun showNoContent() {
        println("NO COINTENT")
    }
}
