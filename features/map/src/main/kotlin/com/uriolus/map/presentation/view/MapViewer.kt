package com.uriolus.map.presentation.view

import android.os.Bundle
import com.uriolus.infrastructure.domain.Picture


interface MapViewer {
    fun createMap() {}

    fun onCreate(savedInstanceState: Bundle?, onMapReady: (() -> Unit)?)
    fun onResume() {}
    fun onStart() {}
    fun onStop() {}
    fun onPause() {}
    fun onLowMemory() {}
    fun onDestroy() {}
    fun onSaveInstanceState(outState: Bundle) {}
    fun renderPictures(pictures: List<Picture>)
}
