package com.uriolus.map.presentation.di

import com.uriolus.map.presentation.presenter.MapPresenter
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider


val featureModule = Kodein.Module(name = "MAP_MODULE") {
    bind<MapPresenter>() with provider { MapPresenter(pictureRepository = instance()) }
}
