package com.uriolus.map.presentation.presenter

import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.PictureRepository
import com.uriolus.infrastructure.domain.exceptions.error.DomainError
import com.uriolus.infrastructure.orchestrator.Orchestrator
import com.uriolus.infrastructure.threading.LifecyclePresenter
import com.uriolus.infrastructure.threading.ViewPresenter
import com.uriolus.permissionchecker.domain.model.PermissionsResult
import kotlinx.coroutines.launch

typealias Pictures = List<Picture>

class MapPresenter(
    val pictureRepository: PictureRepository
) : LifecyclePresenter<MapPresenter.View>() {

    private val picturesOrchestrator = Orchestrator<Pictures>(1)

    override fun initPresenter() {
        scope.launch { doBackgroundWork() }
    }

    private suspend fun doBackgroundWork() {
        view?.checkPermissions()
            ?.fold(
                onGranted = {
                    getPictures()
                },
                onDenied = {
                    view?.renderError(DomainError.PermissionsNotGranted("Not granted"))
                }
            )

        picturesOrchestrator.whenReady { view?.renderPictures(it) }
    }

    fun mapIsReady() {
        picturesOrchestrator.conditionMet()
    }

    private fun getPictures() {
        scope.launch {
            pictureRepository.getAllPictures()
                .fold(
                    ifRight = {
                        println("PICTURES ok: ${it.size}")
                        picturesOrchestrator.conditionMet(it)
                    },
                    ifLeft = {
                        view?.renderError(it)
                    }
                )
        }
    }

    fun onClose() {
        clear()
    }

    interface View : ViewPresenter {
        fun renderPictures(pictures: List<Picture>)
        fun renderError(error: DomainError)
        suspend fun checkPermissions(): PermissionsResult
    }
}
