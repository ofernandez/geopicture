package com.uriolus.map.presentation.view

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.exceptions.error.DomainError
import com.uriolus.map.R
import com.uriolus.map.presentation.di.featureComponent
import com.uriolus.map.presentation.presenter.MapPresenter
import com.uriolus.permissionchecker.PermissionsChecker
import com.uriolus.permissionchecker.domain.model.PermissionsResult
import org.kodein.di.generic.instance


class MapActivity : AppCompatActivity(), MapPresenter.View {


    private val presenter: MapPresenter by featureComponent.instance()
    private lateinit var permissionsChecker: PermissionsChecker
    private val mapViewer: MapViewer = MapBoxMapViewer(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissionsChecker = PermissionsChecker(this)
        mapViewer.createMap()
        setContentView(R.layout.map_layout)
        setUpMap(savedInstanceState)
        presenter.onViewReady(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapViewer.onDestroy()
        presenter.onClose()
    }

    override suspend fun checkPermissions(): PermissionsResult =
        permissionsChecker.checkPermissions(
            arrayListOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        )

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionsChecker.onRequestPermissionsResult(requestCode, permissions, grantResults.toTypedArray())
    }

    override fun renderPictures(pictures: List<Picture>) {
        mapViewer.renderPictures(pictures)
    }

    override fun renderError(error: DomainError) {
        println("RENDER ERROR")
    }

    private fun setUpMap(savedInstanceState: Bundle?) {
        mapViewer.onCreate(savedInstanceState) { presenter.mapIsReady() }
    }
    // Add the mapView lifecycle to the activity's lifecycle methods

    public override fun onResume() {
        super.onResume()
        mapViewer.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapViewer.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapViewer.onStop()
    }

    public override fun onPause() {
        super.onPause()
        mapViewer.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapViewer.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapViewer.onSaveInstanceState(outState)
    }
}
