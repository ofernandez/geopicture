package com.uriolus.map.presentation.view

import android.app.Activity
import android.os.Bundle
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.uriolus.infrastructure.domain.Picture
import com.uriolus.map.R


class MapBoxMapViewer(private val activity: Activity) : MapViewer {
    private lateinit var mapView: MapView
    private var mapboxMap: MapboxMap? = null
    private lateinit var iconFactory: IconFactory
    private val ICON_SOURCE_ID = "ICON_SOURCE_ID"
    private val ICON_LAYER_ID = "ICON_LAYER_ID"

    override fun createMap() {
        Mapbox.getInstance(
            activity,
            "pk.eyJ1IjoidXJpb2x1cyIsImEiOiJjamgyZWxwczcwZXM0MnhyZmkyNDM1dXMzIn0.30C_rcEeUnZCWaoMtWi_xQ"
        )
    }

    override fun onCreate(savedInstanceState: Bundle?, onMapReady: (() -> Unit)?) {
        iconFactory = IconFactory.getInstance(activity)
        mapView = activity.findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            this.mapboxMap = mapboxMap
            mapboxMap.setStyle(Style.OUTDOORS) {
                onMapReady?.invoke()
            }
        }
    }

    override fun onDestroy() {
        mapView.onDestroy()
    }

    override fun onResume() {
        mapView.onResume()
    }

    override fun onStart() {
        mapView.onStart()
    }

    override fun onStop() {
        mapView.onStop()
    }

    override fun onPause() {
        mapView.onPause()
    }

    override fun onLowMemory() {
        mapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        mapView.onSaveInstanceState(outState)
    }

    override fun renderPictures(pictures: List<Picture>) {
        pictures.forEach { picture ->
            picture.geoPoint?.let { geoPoint ->
                val icon = iconFactory.fromPath(picture.path)
                mapboxMap?.addMarker(
                    MarkerOptions()
                        .position(LatLng(geoPoint.lat, geoPoint.long))
                        .title(picture.path)
                        .icon(icon)
                )
            }
        }
    }
}
