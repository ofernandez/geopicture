package com.uriolus.permissionchecker.extension

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.core.os.bundleOf
import kotlin.reflect.KClass

internal fun <T : DialogFragment> T.withArguments(vararg arguments: Pair<String, Any?>): T {
    this.arguments = bundleOf(*arguments)
    return this
}

inline fun <reified T : DialogFragment> DialogFragment.findByTag(block: (T) -> Unit) =
    childFragmentManager.findByTag<T> { block(it) }

inline fun <reified T : DialogFragment> FragmentManager.findByTag(block: (T) -> Unit) {
    val tag = getTag(T::class)
    (this.findFragmentByTag(tag) as? T)?.let { block(it) }
}

fun getTag(type: KClass<*>) = type.java.name


inline fun <reified T : DialogFragment> FragmentTransaction.showDialog(fragment: DialogFragment) {
    val tag = getTag(T::class)
    addToBackStack(null)
    fragment.show(this, tag)
}
