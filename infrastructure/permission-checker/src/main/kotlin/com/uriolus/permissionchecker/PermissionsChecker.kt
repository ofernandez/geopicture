package com.uriolus.permissionchecker

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.uriolus.permissionchecker.domain.model.PermissionsResult
import com.uriolus.permissionchecker.extension.checkPermissions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

class PermissionsChecker(private val activity: Activity) {

    companion object {
        private const val PERMISSION_REQUEST_CODE = 132
    }

    private val channel = Channel<PermissionsResult>()

    private val coroutineScope = CoroutineScope(Job() + Dispatchers.Default)
    suspend fun checkPermissions(
        permissionList: ArrayList<String>
    ): PermissionsResult {
        activity.checkPermissions(permissionList)
            .fold(
                onDenied = {
                    return startPermissionsFlow(permissionList)
                },
                onGranted = {
                    return PermissionsResult.GRANTED
                }
            )
    }

    private suspend fun startPermissionsFlow(
        permissionList: ArrayList<String>
    ): PermissionsResult {
        ActivityCompat.requestPermissions(
            activity,
            permissionList.toTypedArray(),
            PERMISSION_REQUEST_CODE
        )
        return channel.receive()
    }

    fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: Array<Int>
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                val granted = grantResults.filter { it == PackageManager.PERMISSION_GRANTED }
                coroutineScope.launch {
                    val result =
                        if (granted.size == permissions.size) {
                            PermissionsResult.GRANTED
                        } else {
                            PermissionsResult.DENIED
                        }
                    channel.send(result)
                    channel.close()
                }
            }
            else -> {
                println("Request code not valid")
            }
        }
    }
}

