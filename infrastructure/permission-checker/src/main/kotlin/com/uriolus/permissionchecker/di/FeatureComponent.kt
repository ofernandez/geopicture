package com.uriolus.permissionchecker.di

import com.uriolus.infrastructure.di.coreComponent
import org.kodein.di.Kodein


val featureComponent = Kodein {
    extend(coreComponent)
    import(featureModule)
}
