package com.uriolus.permissionchecker.domain.model

sealed class PermissionsResult {
    object GRANTED : PermissionsResult()
    object DENIED : PermissionsResult()

    inline fun <R> map(f: (PermissionsResult) -> R): R = f(this)

    inline fun flatMap(f: (PermissionsResult) -> Unit): PermissionsResult {
        if (this is GRANTED) {
            f(this)
        }
        return this
    }

    inline fun <R> fold(onDenied: () -> R, onGranted: (PermissionsResult) -> R): R =
        if (this is DENIED) {
            onDenied()
        } else {
            onGranted(this)
        }
}
