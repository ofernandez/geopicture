package com.uriolus.permissionchecker.extension

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.uriolus.permissionchecker.domain.model.PermissionsResult


fun Context.checkPermissions(permissions: List<String>): PermissionsResult {
    val permissionsNotGranted = permissions.filter {
        ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
    }
    return if(permissionsNotGranted.isNotEmpty()){
        PermissionsResult.DENIED
    }else{
        PermissionsResult.GRANTED
    }
}

