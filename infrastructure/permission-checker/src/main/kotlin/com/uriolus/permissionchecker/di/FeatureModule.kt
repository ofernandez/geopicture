package com.uriolus.permissionchecker.di

import com.uriolus.permissionchecker.domain.stream.PermissionStream
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

internal val featureModule = Kodein.Module(name = "PERMISSION_MODULE") {
    bind<PermissionStream>() with singleton { PermissionStream() }
}
