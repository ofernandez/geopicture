package com.uriolus.permissionchecker.view

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.uriolus.permissionchecker.di.featureComponent
import com.uriolus.permissionchecker.domain.model.PermissionsResult
import com.uriolus.permissionchecker.domain.stream.PermissionStream
import org.kodein.di.generic.instance

class PermissionCheckerActivity : AppCompatActivity() {

    companion object {
        private const val ARGUMENT_PERMISSIONS = "argument:permissions"
        private const val REQUEST_PERMISSION_CODE = 1232

        fun getIntent(
            context: Context,
            permissionList: ArrayList<String>
        ): Intent {
            val intent = Intent(context, PermissionCheckerActivity::class.java)
            intent.putExtra(ARGUMENT_PERMISSIONS, permissionList)
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
            return intent
        }
    }

    private val stream: PermissionStream by featureComponent.instance()
    private val permissionList: ArrayList<String>
        get() = intent.getSerializableExtra(ARGUMENT_PERMISSIONS) as? ArrayList<String> ?: arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verifyAndRequestNeededPermission()
    }

    private fun verifyAndRequestNeededPermission() {
        if (isPermissionPreviouslyGranted()) {
            onPermissionGranted()
        } else {
            ActivityCompat.requestPermissions(this, permissionList.toTypedArray(), REQUEST_PERMISSION_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (arePermissionGrantedOnResult(
                    permissions,
                    grantResults
                )
            ) {
                onPermissionGranted()
            } else {
                onPermissionDenied()
            }
        }
    }

    private fun isPermissionPreviouslyGranted(): Boolean {
        return checkPermissionsList { permission ->
            ContextCompat.checkSelfPermission(
                this,
                permission
            )
        }
    }

    private fun arePermissionGrantedOnResult(
        permissions: Array<String>,
        grantResults: IntArray
    ): Boolean {
        return checkPermissionsList { permission ->
            val index = permissions.indices.firstOrNull { permissions[it] == permission }
            index?.let { grantResults[index] } ?: PackageManager.PERMISSION_DENIED
        }
    }

    private fun checkPermissionsList(permissionResult: (String) -> Int): Boolean {
        return permissionList
            .map { permission ->
                permissionResult(permission)
            }
            .all { result ->
                result == PackageManager.PERMISSION_GRANTED
            }
    }

    private fun onPermissionGranted() {
        stream.next(PermissionsResult.GRANTED)
        finish()
    }

    private fun onPermissionDenied() {
        stream.next(PermissionsResult.DENIED)
        finish()
    }
}
