package com.uriolus.permissionchecker.domain.stream

import com.uriolus.infrastructure.threading.stream.BehaviourStream
import com.uriolus.permissionchecker.domain.model.PermissionsResult

class PermissionStream : BehaviourStream<PermissionsResult>()
