package com.uriolus.geopictures.navigation.di

import com.uriolus.geopictures.navigation.Navigator
import com.uriolus.geopictures.navigation.NavigatorImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

val navigationModule = Kodein.Module(name = "NAVIGATION_MODULE") {
    bind<Navigator>() with singleton { NavigatorImpl() }


}
