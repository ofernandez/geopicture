package com.uriolus.geopictures.navigation.requisite

import com.uriolus.geopictures.navigation.NavigationCommand

interface Requisite {
    val navigationCommand: NavigationCommand

    fun isAccomplished(): Boolean
}
