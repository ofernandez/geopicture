package com.uriolus.geopictures.navigation.command.grid

import android.net.Uri
import com.uriolus.geopictures.navigation.NavigationCommand
import com.uriolus.geopictures.navigation.command.Command

val gridNavigationCommand: NavigationCommand = { scheme ->
    Command(
        uri = Uri.Builder()
            .scheme(scheme)
            .authority("grid")
            .build()
    )
}
