package com.uriolus.geopictures.navigation.command.about

import android.net.Uri
import com.uriolus.geopictures.navigation.NavigationCommand
import com.uriolus.geopictures.navigation.command.Command

val aboutNavigationCommand: NavigationCommand = { scheme ->
    Command(
        uri = Uri.Builder()
            .scheme(scheme)
            .authority("about")
            .build()
    )
}
