package com.uriolus.geopictures.navigation.command.policy

import android.net.Uri
import com.uriolus.geopictures.navigation.NavigationCommand
import com.uriolus.geopictures.navigation.command.Command

val policyNavigationCommand: (String) -> NavigationCommand = { url ->
    val command: NavigationCommand = {
        val uri = Uri.parse(url)
        Command(uri = uri)
    }
    command
}
