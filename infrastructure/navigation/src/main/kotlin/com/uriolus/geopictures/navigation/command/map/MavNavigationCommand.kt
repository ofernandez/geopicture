package com.uriolus.geopictures.navigation.command.map

import android.net.Uri
import com.uriolus.geopictures.navigation.NavigationCommand
import com.uriolus.geopictures.navigation.command.Command

val mapNavigationCommand: NavigationCommand = { scheme ->
    Command(
        uri = Uri.Builder()
            .scheme(scheme)
            .authority("map")
            .build()
    )
}
