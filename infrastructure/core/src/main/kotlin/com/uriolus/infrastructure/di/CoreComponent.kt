package com.uriolus.infrastructure.di

import org.kodein.di.Kodein


val coreComponent = Kodein.lazy {
    import(coreModule)
}
