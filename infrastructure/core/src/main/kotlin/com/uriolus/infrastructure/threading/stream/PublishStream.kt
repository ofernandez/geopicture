package com.uriolus.infrastructure.threading.stream

import kotlinx.coroutines.channels.BroadcastChannel

abstract class PublishStream<T>(capacity: Int = 1) : Stream<T> {
    private val stream = BroadcastChannel<T>(capacity)

    override fun subscribe() = stream.openSubscription()

    fun next(element: T) {
        stream.offer(element)
    }
}
