package com.uriolus.infrastructure.data.datasource

import android.content.Context
import android.database.Cursor
import android.provider.MediaStore
import android.util.Log
import androidx.exifinterface.media.ExifInterface
import arrow.core.Either
import com.uriolus.infrastructure.domain.GeoPoint
import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.exceptions.error.DomainError
import com.uriolus.infrastructure.utils.getSignForLatitude
import com.uriolus.infrastructure.utils.getSignForLongitude
import com.uriolus.infrastructure.utils.toDoubleDegrees
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

//https://blog.fossasia.org/displaying-all-the-images-from-storage-at-once-in-phimpme-android-application/
class MediaPictureDataSource(val context: Context) : PictureDataSource {

    private val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
    private val projection = arrayOf(
        MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATA,
        MediaStore.MediaColumns.DATE_ADDED
    )

    override suspend fun getAllPictures(): Either<DomainError, List<Picture>> {
        return withContext(Dispatchers.IO) {
            try {
                var counter = 0
                val cursor = context.contentResolver.query(uri, projection, null, null, null)
                val listOfAllImages = mutableListOf<Picture>()
                cursor?.let {
                    while (it.moveToNext() && counter < 10) {//TODO REMOVE COUNTER
                        val picture = it.getPicture()
                        picture?.let {  listOfAllImages.add(it) }
                        counter++
                    }
                    it.close()
                }
                println("Number of pictures ${listOfAllImages.size}")

                Either.right(listOfAllImages)

            } catch (e: Exception) {
                println("PICTURES error $e")
                Either.left(DomainError.NotFound(e.message ?: ""))
            }
        }
    }

    private fun Cursor.getPicture(): Picture? {
        val absolutePathOfImage =
            this.getString(this.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
        val id = this.getString(this.getColumnIndexOrThrow(MediaStore.MediaColumns._ID))
        val date = this.getString(this.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_ADDED))
        val geoPoint = absolutePathOfImage.geoPointFromExif()
        return geoPoint.fold(
            ifRight = { Picture(id, absolutePathOfImage, date, it) },
            ifLeft = { null }
        )
    }

    private fun String.geoPointFromExif(): Either<DomainError, GeoPoint> {
        return try {
            val exifInterface = ExifInterface(this)
            val latitudeExif = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE)
            val latitudeExifRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF)
            val latitudePair = Pair(latitudeExifRef, latitudeExif)
            val longitudeExif = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE)
            val longitudeRefExif = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF)
            val longitudePair = Pair(longitudeRefExif, longitudeExif)
            val latitude = latitudePair.transformLatitudeToDoubleDegrees()
            val longitude = longitudePair.transformLongitudeToDoubleDegrees()
            //Return
            if (latitude.isValidCoordinate() && longitude.isValidCoordinate()) {
                Either.right(GeoPoint(latitude!!, longitude!!))
            } else {
                Either.left(DomainError.ExifNumberParsing("$latitudeExif::$longitudeExif"))
            }
        } catch (e: IOException) {
            Log.e("PICTURES", "IO Exception ${e.message} $this")
            Either.left(DomainError.IOError(e.message))
        }
    }

    private fun Double?.isValidCoordinate(): Boolean =
        this != null && this != 0.0

    private fun Pair<String?, String?>.transformLatitudeToDoubleDegrees(): Double? {
        if (first == null || second == null) return null
        return second!!.toDoubleDegrees().fold(
            ifLeft = { null },
            ifRight = { it * first!!.getSignForLatitude() }
        )
    }

    private fun Pair<String?, String?>.transformLongitudeToDoubleDegrees(): Double? {
        if (first == null || second == null) return null
        return second!!.toDoubleDegrees().fold(
            ifLeft = { null },
            ifRight = { it * first!!.getSignForLongitude() }
        )
    }
}
