package com.uriolus.infrastructure.orchestrator

import kotlinx.coroutines.CompletableDeferred


class Orchestrator<T>(preconditions: Int = 0, private var value: T? = null) {

    private val preconditionsList = Array(preconditions) { false }
    private var deferredUntilConnected = CompletableDeferred<T>()

    fun conditionMet() {
        val index = preconditionsList.indexOf(false)
        if (index > -1 && index < preconditionsList.size) {
            preconditionsList[index] = true
        }
        check()
    }

    fun conditionMet(value: T) {
        this.value = value
        check()
    }

    private fun check() {
        if (preconditionsList.firstOrNull { !it } == null && value != null) {
            value?.let {
                deferredUntilConnected.complete(it)
            }
        }
    }

    suspend fun awaitTillReady(): T {
        return deferredUntilConnected.await()
    }

    suspend fun whenReady(block: (T) -> Unit) {
        val value = deferredUntilConnected.await()
        block(value)
    }
}
