package com.uriolus.infrastructure.di

import android.app.Application
import android.content.Context
import com.uriolus.infrastructure.data.di.dataModule
import com.uriolus.infrastructure.domain.exceptions.ContextException
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

private var APPLICATION_CONTEXT: Context? = null

val coreModule = Kodein.Module(name = "APPLICATION_MODULE") {
    import(dataModule)
    bind<Context>() with singleton {
        APPLICATION_CONTEXT ?: throw ContextException()
    }
}

fun Application.setApplicationContext() {
    APPLICATION_CONTEXT = this
}
