package com.uriolus.infrastructure.domain.exceptions.error


sealed class DomainError(msg: String? = "") {
    class PermissionsNotGranted(msg: String?) : DomainError(msg)
    class NotFound(msg: String?) : DomainError(msg)
    class IOError(msg: String?) : DomainError(msg)
    class ExifNumberParsing(msg: String?) : DomainError(msg)
}
