package com.uriolus.infrastructure.utils

import android.util.Log
import arrow.core.Either
import com.uriolus.infrastructure.domain.exceptions.error.DomainError


fun String.getSignForLatitude(): Int {
    return if (this == "N") {
        1
    } else {
        -1
    }
}

fun String.getSignForLongitude(): Int {
    return if (this == "E") {
        1
    } else {
        -1
    }
}

 fun String.toDoubleDegrees(): Either<DomainError, Double> {
    return try {
        val number = convertToDegree(this)
        number?.let {
            Either.right(number)
        } ?: Either.left(DomainError.ExifNumberParsing(this))
    } catch (num: NumberFormatException) {
        Log.e("PICTURES", "Parsing exception with string:$this")
        Either.left(DomainError.ExifNumberParsing(this))
    }
}

fun convertToDegree(stringDMS: String): Double? {
    var result: Double? = null
    val DMS = stringDMS.split(",".toRegex(), 3).toTypedArray()

    val stringD = DMS[0].split("/".toRegex(), 2).toTypedArray()
    val D0 = stringD[0].toDouble()
    val D1 = stringD[1].toDouble()
    val FloatD = D0 / D1

    val stringM = DMS[1].split("/".toRegex(), 2).toTypedArray()
    val M0 = stringM[0].toDouble()
    val M1 = stringM[1].toDouble()
    val FloatM = M0 / M1

    val stringS = DMS[2].split("/".toRegex(), 2).toTypedArray()
    val S0 = stringS[0].toDouble()
    val S1 = stringS[1].toDouble()
    val FloatS = S0 / S1

    result = FloatD + FloatM / 60 + FloatS / 3600

    return result
}
