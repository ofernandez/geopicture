package com.uriolus.infrastructure.threading

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel


abstract class LifecyclePresenter<T : ViewPresenter> {
    protected var view: T? = null
    private val job = SupervisorJob()
    protected val scope = CoroutineScope(job + Dispatchers.Main)

    fun onViewReady(view: T) {
        this.view = view
        initPresenter()
    }

    fun clear() {
        scope.cancel()
    }

    abstract fun initPresenter()
}

interface ViewPresenter
