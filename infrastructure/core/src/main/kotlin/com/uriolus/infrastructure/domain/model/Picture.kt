package com.uriolus.infrastructure.domain


data class Picture(val id: String, val path: String, val date: String?, val geoPoint: GeoPoint?)

data class GeoPoint(val lat: Double, val long: Double)
