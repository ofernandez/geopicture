package com.uriolus.infrastructure.log

import android.util.Log


interface Logging {
    fun d(msg: String)
    fun e(msg: String)
}

class SystemLog() : Logging {
    override fun d(msg: String) {
        Log.d("PICTURE", msg)
    }

    override fun e(msg: String) {
        Log.e("PICTURE", msg)
    }
}
