package com.uriolus.infrastructure.data.repository

import arrow.core.Either
import com.uriolus.infrastructure.data.datasource.PictureDataSource
import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.PictureRepository
import com.uriolus.infrastructure.domain.exceptions.error.DomainError


class MediaPictureRepository(private val dataSource: PictureDataSource) : PictureRepository {
    override suspend fun getAllPictures(): Either<DomainError, List<Picture>> {
        return dataSource.getAllPictures()
    }

}
