package com.uriolus.infrastructure.domain

import arrow.core.Either
import com.uriolus.infrastructure.domain.exceptions.error.DomainError


interface PictureRepository {
    suspend fun getAllPictures(): Either<DomainError, List<Picture>>
}
