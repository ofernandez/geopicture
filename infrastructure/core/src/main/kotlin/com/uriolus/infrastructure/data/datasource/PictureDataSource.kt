package com.uriolus.infrastructure.data.datasource

import arrow.core.Either
import com.uriolus.infrastructure.domain.Picture
import com.uriolus.infrastructure.domain.exceptions.error.DomainError


interface PictureDataSource {
    suspend fun getAllPictures(): Either<DomainError, List<Picture>>
}
