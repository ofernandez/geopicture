package com.uriolus.infrastructure.data.di

import com.uriolus.infrastructure.data.datasource.MediaPictureDataSource
import com.uriolus.infrastructure.data.datasource.PictureDataSource
import com.uriolus.infrastructure.data.repository.MediaPictureRepository
import com.uriolus.infrastructure.domain.PictureRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


val dataModule = Kodein.Module(name = "DATA_MODULE") {
    bind<PictureDataSource>() with singleton { MediaPictureDataSource(context = instance()) }
    bind<PictureRepository>() with provider {
        MediaPictureRepository(instance())
    }
}
