package com.uriolus.infrastructure.orchestrator

import kotlinx.coroutines.runBlocking
import org.junit.Test

internal class OrchestratorTest {

    @Test
    fun `when all conditions are  met orchestrator does  return`() {
        val orchestrator = Orchestrator<String>(5)
        orchestrator.conditionMet()
        orchestrator.conditionMet()
        orchestrator.conditionMet()
        orchestrator.conditionMet()
        orchestrator.conditionMet()
        runBlocking {
            orchestrator.conditionMet("Hola")
            val result = orchestrator.awaitTillReady()
            assert(result == "Hola")
        }
    }
}
