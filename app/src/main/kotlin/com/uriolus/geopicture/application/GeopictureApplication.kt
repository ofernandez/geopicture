package com.uriolus.geopicture.application

import android.app.Application
import com.uriolus.infrastructure.di.coreComponent
import com.uriolus.infrastructure.di.setApplicationContext
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware


class GeopictureApplication : Application(), KodeinAware {

    override fun onCreate() {
        super.onCreate()
        setApplicationContext()
    }

    override val kodein = Kodein.lazy {
        extend(coreComponent)
    }
}
